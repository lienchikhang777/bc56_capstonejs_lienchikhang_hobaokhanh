export default class Validate {
    maxiumPrice = 200000000;
    isntNull(selector, value) {
        if (!value) {
            document.querySelector(`small[data-is-null="${selector}"]`).innerText = `${selector} không thể bỏ trống`
            return false;
        }
        document.querySelector(`small[data-is-null="${selector}"]`).innerText = ``
        return true;
    };
    isValidPrice(selector, value) {
        if (value <= 1000000 || value >= this.maxiumPrice) {
            document.querySelector(`small[is-valid-price="${selector}"]`).innerText = `${selector} không được nhỏ hơn 1.000.000 và lớn hơn 200.000.000`
            return false;
        }
        document.querySelector(`small[is-valid-price="${selector}"]`).innerText = ``
        return true;
    };
    isValidLength(selector, value) {
        if (value.length <= 6 || value.length >= 100) {
            document.querySelector(`small[is-valid-length="${selector}"]`).innerText = `${selector} không được nhỏ hơn 6 kí tự và lớn hơn 100 kí tự`
            return false;
        }
        document.querySelector(`small[is-valid-length="${selector}"]`).innerText = ``
        return true;
    }
}