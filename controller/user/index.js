//import
import productService from "../services/productService.js";
import {
    renderProductList,
    renderCartList,
    updateAmountCart,
    deleteFromCart,
    onLoading,
    closeLoading,
    onModalSuccess,
    localStore,
    stringToSlug,
    sleepDialog
} from "./methods.js";
import cartService from "../services/cartService.js";


let start = () => {
    fetchProductList();
    fetchCartList();
}


function fetchProductList() {
    onLoading();
    productService
        .getProductList()
        .then(res => {
            renderProductList(res.data);
            closeLoading();
        })
        .catch(err => {
            console.log(err)
            closeLoading();
        })
}

let fetchCartList = () => {
    onLoading();
    cartService
        .getCartList()
        .then(res => {
            renderCartList(res.data);
            closeLoading();
        })
        .catch(err => {
            console.log(err)
            closeLoading();
        })
}

window.addToCart = (productID) => {
    onLoading();
    //kiem tra productID da ton tai hay chua
    cartService
        .getCartList()
        .then(res => {
            return res.data.map(cart => {
                return cart.productID
            })
        })
        .then(carts => {
            console.log("cart sau khi them trung:", carts)
            let isExistIndex = carts.some(cart => {
                return cart == productID;
            })
            return isExistIndex;
        })
        .then(data => {
            //neu chua ton tai productID
            if (!data) {
                cartService
                    .addCartItem({
                        productID: productID,
                        amount: 1
                    })
                    .then(res => {
                        console.log(res)
                        cartService
                            .getCartList()
                            .then(res => {
                                console.log("cart", res)
                                renderCartList(res.data);
                                closeLoading();
                                onModalSuccess("Đã thêm vào giỏ hàng!");
                            })
                            .catch(err => {
                                console.log(err)
                                closeLoading();
                            })
                    })
                    .catch(err => {
                        console.log(err)
                    })
            }
            //neu da ton tai
            else {
                console.log(data)
                console.log("trung ne adasdasdsa")
                updateAmountCart(productID, true);
                closeLoading();
                onModalSuccess("Đã thêm vào giỏ hàng!");
            }
        })
        .catch(err => {
            console.log(err)
            closeLoading();
        })


}

window.pay = () => {
    const TIME_BETWEEN_FETCHING_DATA = 1500;
    const ESTIMATED_TIME = 500;
    onLoading();
    cartService
        .getCartList()
        .then(res => {
            console.log("length", res.data.length)
            return res.data.map(cart => {
                return cart.id;
            })
        })
        .then(data => {
            // sleepDialog(TIME_BETWEEN_FETCHING_DATA + ESTIMATED_TIME * data.length);
            let curIndex = 0;
            let process = () => {
                if (curIndex < data.length) {
                    sleepDialog(ESTIMATED_TIME * data.length);
                    const cartItem = data[curIndex];
                    cartService
                        .deleteCart(cartItem)
                        .then(res => {
                            console.log(res)
                            curIndex++
                            setTimeout(process, TIME_BETWEEN_FETCHING_DATA);
                        })
                        .catch(err => {
                            console.log(err)
                        })
                }
                else {
                    fetchCartList();
                    onModalSuccess('THANH TOÁN THÀNH CÔNG')
                    closeLoading();
                }
            }
            process();
        })
        .catch(err => {
            console.log(err)
            closeLoading();
        })
}

window.increaseAmount = (productID) => {
    updateAmountCart(productID, true);
}

window.reduceAmount = (productID) => {
    updateAmountCart(productID, false);
}

window.deleteCartItem = (productID) => {
    deleteFromCart(productID);
}

document.querySelector('#select-type').onchange = function () {
    onLoading();
    let curChoose = document.querySelector('#select-type').value;
    console.log(curChoose)
    if (curChoose == 'all') {
        productService
            .getProductList()
            .then(res => {
                renderProductList(res.data);
                closeLoading();
            })
            .catch(err => {
                console.log(err)
                closeLoading();
            })
        return;
    }
    productService
        .getProductList()
        .then(res => {
            return res.data.filter(product => {
                let productItem = {
                    ...product,
                    type: stringToSlug(product.type)
                }
                return productItem.type == curChoose
            })
        })
        .then(filteredProducts => {
            renderProductList(filteredProducts);
            closeLoading();
        })
        .catch(err => {
            console.log(err)
            closeLoading();
        })
}
start();





