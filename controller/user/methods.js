let total = 0;

//import
import productService from "../services/productService.js";
import cartService from "../services/cartService.js";

//getProductList
export let renderProductList = (products) => {
    document.querySelector('#productList').innerHTML = products.map(product => {
        return `
        <div class="product__item">
            <div className="product__top">
                <h2 class="text-xl font-medium">${product.name}</h2>
            </div>
            <div class="product__body">
                <img src="${product.img}" alt="">
                <div class="sub-wrapper">
                <div class="product__sub">
                    <p>${product.desc}</p>
                    <p>Front camera: <span>${product.frontCamera}</span></p>
                    <p>Front camera: <span>${product.backCamera}</span></p>
                    <p>screen: <span>${product.screen}</span></p>
                    <p>type: <span>${product.type}</span></p>
                    <button onclick="addToCart(${product.id})">Thêm</button>
                </div>
            </div>
            </div>
            <div class="product__footer">
                <p>${product.price.toLocaleString()} vnd</p>
            </div>
        </div>
        `
    }).join(' ');
}

export let renderCartList = (carts) => {
    let str = '';
    let cartArray = [];
    if (carts.length == 0) {
        document.querySelector('#cart__wrapper').innerHTML = '';
        document.querySelector('#sumPrice').innerHTML = 0;
        document.querySelector('#productAmount').innerHTML = 0;
        document.querySelector('#cartCount').innerHTML = 0;
        closeLoading();
    }
    carts.forEach(cart => {
        productService
            .getProductByID(cart.productID)
            .then(res => {
                let cartItem = {
                    ...res.data,
                    amount: cart.amount,
                    cartId: cart.id
                }
                cartArray.push(cartItem);
            })
            .then(res => {
                str = cartArray.map((product, index) => {
                    return `
                        <div class="cartItem flex items-center justify-around">
                            <img src="${product.img}" alt="" style="width: 50px; height: 50px;"/>
                            <div class="cartItem__info">
                                <h4>${product.name}</h4>
                                <p>${product.price}</p>
                            </div>
                            <div class="cartItem__editAmount">
                                <button onclick="increaseAmount(${product.id})">
                                    +
                                </button>
                                <span>${product.amount}</span>
                                <button onclick="reduceAmount(${product.id})">
                                    -
                                </button>
                            </div>
                            <div class="cartItem__method">
                                <button onclick="deleteCartItem(${product.cartId})">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    `
                }).join(' ');
            })
            //sau khi có được chuỗi str html thì render 1 lần
            .then(res => {
                //render cart list
                document.querySelector('#cart__wrapper').innerHTML = str;
                //update cart icon
                updateAmountUI(cartArray, '#productAmount');
                updateAmountUI(cartArray, '#cartCount');
                updateTotalPrice(cartArray);
                closeLoading();
            })
            .catch(err => {
                console.log(err)
            })
    })
}

let updateAmountUI = (cartArray, selector) => {
    let num = 0;
    cartArray.forEach(item => {
        num += item.amount;
    })
    document.querySelector(selector).innerHTML = num;
}


export let updateAmountCart = (productID, isIncrease) => {
    onLoading();
    cartService
        .getCartList()
        .then(res => {
            console.log("truoc khi tang", res.data)
            let findedCart = res.data.find(cart => {
                return cart.productID == productID
            })
            let curAmount = findedCart.amount;
            cartService.updateCartAmount(findedCart.id, {
                ...findedCart,
                amount: isIncrease == true ? curAmount += 1 : (curAmount == 1 ? curAmount : curAmount -= 1)
            })
                .then(res => {
                    promiseSleep()
                        .then(res => {
                            console.log(res)
                            cartService
                                .getCartList()
                                .then(res => {
                                    console.log("sau khi tang", res.data)
                                    renderCartList(res.data);
                                    closeLoading();
                                })
                                .catch(err => {
                                    console.log(err)
                                    closeLoading();
                                })
                        })
                        .catch(err => {
                            console.log(err)
                        })
                })
                .catch(err => {
                    console.log(err)
                    closeLoading();
                })
        })
        .catch(err => {
            console.log(err)
        })
        .catch(err => {
            console.log(err)
        })
}


export let deleteFromCart = (productID) => {
    onLoading();
    cartService
        .deleteCart(productID)
        .then(res => {
            cartService.getCartList()
                .then(res => {
                    console.log("mang", res)
                    renderCartList(res.data);
                    onModalSuccess("Đã xoá khỏi giỏ hàng!")
                })
                .catch(err => {
                    console.log(err)
                    closeLoading();
                })
        })
        .catch(err => {
            console.log(err)
        })
}


function updateTotalPrice(cartArray) {
    let sum = cartArray.reduce((finalSum, curVar) => {
        return finalSum + curVar.price * curVar.amount;
    }, 0)
    document.querySelector('#sumPrice').innerHTML = sum;
}

export let onLoading = () => {
    document.querySelector('#loading').style.display = 'flex';
}


export let closeLoading = () => {
    document.querySelector('#loading').style.display = 'none';
}

export let onModalSuccess = (message) => {
    Swal.fire(
        message,
        '',
        'success'
    )
}

export let isExist = (indexList, productID) => {
    return indexList.some(indexItem => {
        return indexItem == productID
    })
}

export let localStore = {
    saveIndexList: (indexList) => {
        localStorage.setItem("indexList", JSON.stringify(indexList));
    },
    getIndexList: () => {
        return JSON.parse(localStorage.getItem("indexList"));
    },
    removeIndexList: () => {
        localStorage.removeItem("indexList");
    }
}


export function stringToSlug(title) {
    //Đổi chữ hoa thành chữ thường
    let slug = title.toLowerCase();

    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');

    return slug;
}

export let sleepDialog = (timer) => {
    let timerInterval
    Swal.fire({
        title: 'ĐANG THANH TOÁN',
        html: 'Vui lòng đợi trong <b></b> milliseconds.',
        timer,
        timerProgressBar: true,
        didOpen: () => {
            Swal.showLoading()
            const b = Swal.getHtmlContainer().querySelector('b')
            timerInterval = setInterval(() => {
                b.textContent = Swal.getTimerLeft()
            }, 100)
        },
        willClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
        }
    })
}

let promiseSleep = () => {
    return new Promise((resolve) => {
        setTimeout(resolve, 1000)
    })
}