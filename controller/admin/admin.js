//import
import productService from "../services/productService.js";
import { renderProductList, getFormDataWithoutID, closeModal, fillFormData, getFormDataWithID } from "./adminMethods.js";
import { onLoading, closeLoading, onModalSuccess } from "../user/methods.js";
import Validate from '../../models/Validate.js'
import { fetchProducts, checkValidate, SortedProducts } from './adminMethods.js';
let fetchingProductData = () => {
    onLoading();
    productService
        .getProductList()
        .then(res => {
            console.log(res.data);
            renderProductList(res.data);
        })
        .catch(err => {
            console.log(err)
        })
        .finally(() => {
            closeLoading();
        })
}


let addProduct = () => {
    onLoading();
    let product = getFormDataWithoutID();

    let isValid = checkValidate(product);

    if (!isValid) {
        return;
    }

    productService
        .addNewProduct(product)
        .then(res => {
            fetchingProductData();
        })
        .catch(err => {
            console.log(err)
        })
        .finally(() => {
            closeLoading();
            closeModal();
            onModalSuccess('Thêm sản phẩm thành công');
        })

}



window.loadBaseConfig = () => {
    document.querySelector('#edit-btn').style.display = 'none';
    document.querySelector('#add-btn').style.display = 'block';
    document.querySelector('#id-form').style.display = 'none';
}

let changeConfig = () => {
    document.querySelector('#edit-btn').style.display = 'block';
    document.querySelector('#id-form').style.display = 'block';
    document.querySelector('#add-btn').style.display = 'none';
}

window.deleteProduct = (productID) => {
    onLoading();
    console.log(productID)
    productService
        .deleteProduct(productID)
        .then(res => {
            fetchingProductData();
        })
        .catch(err => {
            console.log(err)
        })
        .finally(() => {
            closeLoading();
            onModalSuccess('Xoa thanh cong');
        })
}


window.openModal = (productID) => {
    onLoading();
    $('#myModal').modal('show');
    changeConfig();
    productService
        .getProductByID(productID)
        .then(res => {
            console.log(res.data)
            fillFormData(res.data)
        })
        .catch(err => {
            console.log(err)
        })
        .finally(() => {
            closeLoading();
        })
}

let updateProduct = () => {
    onLoading();
    let product = getFormDataWithID();
    let isValid = checkValidate(product);

    if (!isValid) {
        return;
    }
    productService
        .updateProduct(product.id, product)
        .then(res => {
            console.log(res)
            fetchingProductData();
        })
        .catch(err => {
            console.log(err)
        })
        .finally(() => {
            closeLoading();
            onModalSuccess('CAP NHAT THANH CONG');
            closeModal();
        })
}


document.querySelector('#add-btn').onclick = addProduct;
document.querySelector('#edit-btn').onclick = updateProduct;

let start = () => {
    fetchingProductData();
    loadBaseConfig();
}

start();


// searchproduct
export let searchProductsByName = async (name) => {
    const products = await fetchProducts();
    return products.filter(product => product.name.toLowerCase() === name.toLowerCase());
};

window.SortedProducts = () => {
    SortedProducts();
}

window.searchProduct = async () => {
    const name = document.getElementById('searchInput').value;
    const productList = document.getElementById('productList');
    productList.innerHTML = ''; // Clear previous results

    try {
        let results = await searchProductsByName(name);
        results.forEach(product => {
            const listItem = document.createElement('li');
            listItem.className = 'list-group-item';
            listItem.textContent = product.name;
            productList.appendChild(listItem);
        });
    } catch (error) {
        console.error('Error:', error);
    }
};