import Validate from "../../models/Validate.js";
import productService from '../services/productService.js'

export let renderProductList = (products) => {
    document.querySelector('#product-list').innerHTML = products.reverse().map(product => {
        return `
        <tr>
            <td>${product.id}</td>
            <td>${product.name}</td>
            <td>${product.price.toLocaleString()}</td>
            <td>${product.screen}</td>
            <td>${product.backCamera}</td>
            <td>${product.frontCamera}</td>
            <td>
                <img src="${product.img}" alt="" width=200px height=200px/>
            </td>
            <td>${product.desc}</td>
            <td>${product.type}</td>
            <td>
                <button onclick="deleteProduct(${product.id})">Delete</button>
                <button onclick="openModal(${product.id})">Edit</button>
            </td>
        </tr>
        `
    }).join(' ');
}

export let getFormDataWithoutID = () => {
    return {
        name: document.querySelector('#name').value,
        price: document.querySelector('#price').value * 1,
        screen: document.querySelector('#screen').value,
        backCamera: document.querySelector('#backCamera').value,
        frontCamera: document.querySelector('#frontCamera').value,
        img: document.querySelector('#img').value,
        desc: document.querySelector('#desc').value,
        type: document.querySelector('#type').value,
    }
}

export let getFormDataWithID = () => {
    return {
        id: document.querySelector('#id').value,
        ...getFormDataWithoutID()
    }
}

export let checkValidate = (product) => {
    let validate = new Validate();

    let isValid = validate.isntNull('name', product.name)
        & validate.isntNull('price', product.price)
        & validate.isntNull('screen', product.screen)
        & validate.isntNull('backCamera', product.backCamera)
        & validate.isntNull('frontCamera', product.frontCamera)
        & validate.isntNull('desc', product.desc)
        & validate.isntNull('img', product.img)
        & validate.isValidPrice('price', product.price)
        & validate.isValidLength('name', product.name)
        & validate.isValidLength('backCamera', product.backCamera)
        & validate.isValidLength('frontCamera', product.frontCamera)

    return isValid;
}

export let fillFormData = (product) => {
    document.querySelector('#id').value = product.id;
    document.querySelector('#name').value = product.name;
    document.querySelector('#price').value = product.price;
    document.querySelector('#screen').value = product.screen;
    document.querySelector('#backCamera').value = product.backCamera;
    document.querySelector('#frontCamera').value = product.frontCamera;
    document.querySelector('#img').value = product.img;
    document.querySelector('#desc').value = product.desc;
    document.querySelector('#type').value = product.type;
}

export let closeModal = () => {
    $('#myModal').modal('hide')
}


// ProductSearching

// function searchProduct() {
//     let query = document.getElementById('searchBox').value.toLowerCase();
//     if (!query) return;
//     fetch('https://64d9e357e947d30a260a6dc8.mockapi.io/Products')
//         .then(response => response.json())
//         .then(data => {
//             let filteredProducts = data.filter(product => product.name.toLowerCase().includes(query));
//             displayResults(filteredProducts);
//         })
//         .catch(error => console.error("Error:", error));
// }

// function displayResults(products) {
//     let resultsDiv = document.getElementById('results');
//     resultsDiv.innerHTML = '';  // Clear previous results

//     products.forEach(product => {
//         let listItem = document.createElement('li');
//         listItem.className = 'list-group-item';
//         listItem.textContent = product.name;
//         resultsDiv.appendChild(listItem);
//     });
// }
export let fetchProducts = async () => {
    const response = await fetch('https://64d9e357e947d30a260a6dc8.mockapi.io/Products');
    if (!response.ok) {
        throw new Error('Khong tim thay san pham');
    }
    return await response.json();
};

export let SortedProducts = () => {
    const sortOrder = document.getElementById('sortOrder').value;
    console.log('yes')
    productService
        .getProductList()
        .then(res => {
            let sortProducts;
            if (sortOrder === 'asc') {
                sortProducts = res.data.sort((a, b) => a.price - b.price);
                console.log(sortProducts)
            } else {
                sortProducts = res.data.sort((a, b) => b.price - a.price);
                console.log(sortProducts)
            }
            renderProductList(sortProducts);
        })
        .catch(err => {
            console.log(err);
        })
}
let start = () => {
    fetchAndDisplaySortedProducts();
    loadBaseConfig();
}