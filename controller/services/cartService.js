const CART_API = `https://64d9e357e947d30a260a6dc8.mockapi.io/Carts`


//addCart
let addCartItem = (cartData) => {
    return axios({
        url: CART_API,
        method: "POST",
        data: cartData
    })
}

//getCartList
let getCartList = () => {
    return axios({
        url: CART_API,
        method: "GET"
    })
}

//updateCartAmount
let updateCartAmount = (cartID, newAmount) => {
    return axios({
        url: CART_API + `/${cartID}`,
        method: "PUT",
        data: newAmount
    })
}

//deleteCart
let deleteCart = (cartID) => {
    return axios({
        url: CART_API + `/${cartID}`,
        method: "DELETE"
    })
}

//deleteMultiCart
let deleteMultiCart = (cartsID) => {
    let deleteRequest = cartsID.map(cartid => {
        return {
            url: CART_API + `/${cartid}`,
            method: "DELETE"
        }
    })
    return axios.all(deleteRequest.map(request => axios(request)));
}

//getCart
let getCartItem = (cartID) => {
    return axios({
        url: CART_API + `/${cartID}`,
        method: "GET"
    })
}



let cartService = {
    addCartItem,
    getCartList,
    updateCartAmount,
    deleteCart,
    getCartItem,
    deleteMultiCart
}

export default cartService;