const PRODUCT_API = `https://64d9e357e947d30a260a6dc8.mockapi.io/Products`;

//getProductList
let getProductList = () => {
    return axios({
        url: PRODUCT_API,
        method: "GET"
    })
}

//getProductByID
let getProductByID = (productID) => {
    return axios({
        url: PRODUCT_API + `/${productID}`,
        method: "GET"
    })
}

//adding
let addNewProduct = (data) => {
    return axios({
        url: PRODUCT_API,
        method: "POST",
        data: data
    })
}

//delete
let deleteProduct = (productID) => {
    return axios({
        url: PRODUCT_API + `/${productID}`,
        method: "DELETE"
    })
}

//update
let updateProduct = (productID, data) => {
    return axios({
        url: PRODUCT_API + `/${productID}`,
        method: "PUT",
        data: data
    })
}

let productService = {
    getProductList,
    getProductByID,
    addNewProduct,
    deleteProduct,
    updateProduct
}

export default productService;